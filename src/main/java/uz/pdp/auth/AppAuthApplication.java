package uz.pdp.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import uz.pdp.auth.entity.User;
import uz.pdp.auth.repository.RoleRepository;
import uz.pdp.auth.repository.UserRepository;

@SpringBootApplication
public class AppAuthApplication implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Value("${spring.datasource.initialization-mode}")
    String initMode;

    public static void main(String[] args) {
        SpringApplication.run(AppAuthApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")){
            User user = new User(
                    "coderuz",
                    "Coder Uzbek",
                    "+998998067786",
                    "coderuz.2019@gmail.com",
                    passwordEncoder.encode("123")
            );
            user.setRoles(roleRepository.findAll());
            user.setEnabled(true);
            userRepository.save(user);
        }
    }
}
