package uz.pdp.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import uz.pdp.auth.entity.Role;
import uz.pdp.auth.entity.User;
import uz.pdp.auth.repository.RoleRepository;
import uz.pdp.auth.repository.UserRepository;


import java.util.List;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Override
    public UserDetails loadUserByUsername(String usernameOrEmailOrPhone) throws UsernameNotFoundException {
        List<Role> roles=roleRepository.findAll();
        roles.forEach(role -> System.out.println(role));
        User user=userRepository.findByUsernameOrEmailOrPhone(usernameOrEmailOrPhone,usernameOrEmailOrPhone,usernameOrEmailOrPhone).get();
//        return userRepository.findByUsernameOrEmailOrPhone(usernameOrEmailOrPhone,usernameOrEmailOrPhone,usernameOrEmailOrPhone)
//                .orElseThrow(()->new UsernameNotFoundException(usernameOrEmailOrPhone +"  not found!!"));
        System.out.println(user);
        return user;
    }
}
