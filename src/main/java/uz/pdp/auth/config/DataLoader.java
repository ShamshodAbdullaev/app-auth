//package uz.pdp.auth.config;

//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//import uz.pdp.auth.entity.User;
//import uz.pdp.auth.repository.RoleRepository;
//import uz.pdp.auth.repository.UserRepository;

//@Component
//public class DataLoader implements CommandLineRunner {
//    @Autowired
//    UserRepository userRepository;
//    @Autowired
//    RoleRepository roleRepository;
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Value("${spring.datasource.initialization-mode}")
//    String initMode;

//    @Override
//    public void run(String... args) throws Exception {
//        if (initMode.equals("always")){
//            User user = new User(
//                    "coderuz",
//                    "Coder Uzbek",
//                    "+998998067786",
//                    "coderuz.2019@gmail.com",
//                    passwordEncoder.encode("123")
//            );
//            user.setRoles(roleRepository.findAll());
//            user.setEnabled(true);
//            userRepository.save(user);
//        }
//    }
//}
