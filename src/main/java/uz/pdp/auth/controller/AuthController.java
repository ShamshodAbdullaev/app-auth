package uz.pdp.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import uz.pdp.auth.entity.Role;
import uz.pdp.auth.entity.User;
import uz.pdp.auth.model.Result;
import uz.pdp.auth.payload.RegisterReq;
import uz.pdp.auth.repository.RoleRepository;
import uz.pdp.auth.repository.UserRepository;
import uz.pdp.auth.security.CurrentUser;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
public class AuthController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    JavaMailSender javaMailSender;
    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping("/login")
     public String getLoginPage(){
        return "login";
    }



    @GetMapping("/cabinet")
     public String getCabinetPage(Model model, @CurrentUser User user){
//       User user= (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
       model.addAttribute("user",user);
        return "cabinet";
    }
    @GetMapping("/register")
    public String getRegisterPage(Model model){
        model.addAttribute("registerReq",new RegisterReq());
        return "register";
    }
    @PostMapping("/register")
    public String login(@Valid RegisterReq registerReq, Model model, BindingResult bindingResult){

        Result result=new Result();
        if (!bindingResult.hasErrors()) {
            Optional<User> optional = userRepository.findByUsernameOrEmailOrPhone(
                    registerReq.getUsername(), registerReq.getEmail(), registerReq.getPhone());
            if (!optional.isPresent()) {
                String activeCode = UUID.randomUUID().toString();
                User user = new User();
                user.setUsername(registerReq.getUsername());
                user.setEmail(registerReq.getEmail());
                user.setPhone(registerReq.getPhone());
                user.setPassword(passwordEncoder.encode(registerReq.getPassword()));
                user.setFullname(registerReq.getFullname());
                user.setActiveCode(activeCode);
                List<Role> roles=roleRepository.findAll();
                user.setRoles(roles);
                userRepository.save(user);

                SimpleMailMessage mailMessage = new SimpleMailMessage();
                mailMessage.setSubject("Verify your new account ");
                mailMessage.setText("Click here for activate your account. " +
                        "http://localhost/activate?activationCode="
                        + user.getActiveCode());
                mailMessage.setTo(user.getEmail());
                javaMailSender.send(mailMessage);
                result.setSuccess(true);
                result.setMessage("Successfully registered!!");
                model.addAttribute("result", result);
                return "/login";
            }
            result.setMessage("User allready exists!!");
            model.addAttribute("result", result);
        }
        model.addAttribute("registerReq",registerReq);
        return "/register";
    }

    @GetMapping("/activate")
    public String activateUser(HttpServletRequest request){
        String activationCode=request.getParameter("activationCode");
        Optional<User> optional=userRepository.findByActiveCode(activationCode);
        if (optional.isPresent()){
            User user=optional.get();
            user.setActiveCode("");
            user.setEnabled(true);
            userRepository.save(user);
        }
        return "login";
    }

}
