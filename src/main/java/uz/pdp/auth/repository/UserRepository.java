package uz.pdp.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.auth.entity.User;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByUsernameOrEmailOrPhone(String username,String email,String phone);
    Optional<User> findByActiveCode(String activationCode);
    Optional<User> findByUsernameOrEmailOrPhoneAndPassword(String username,String email,String phone,String password);
}
